%{!?tcl_version: %global tcl_version %(echo 'puts $tcl_version' | tclsh)}
Name: tix
Summary: the Tk Interface eXtension
Epoch: 1
Version: 8.4.3
Release: 27
License: TCL
URL: http://tix.sourceforge.net/
Source0: http://downloads.sourceforge.net/project/%{name}/%{name}/%{version}/Tix%{version}-src.tar.gz

Patch0: tix-8.4.2-link.patch
Patch1: tix-8.4.3-tcl86.patch
Patch2: tix-8.4.3-covscan-fixes.patch
Patch3: tix-Add-sw64-architecture.patch

Buildrequires: tcl-devel >= 8.4.13 tk-devel >= 8.4.13 libX11-devel gcc

Requires: tcl(abi) = 8.6 tcl >= 8.4.13 tk >= 8.4.13 /etc/ld.so.conf.d

%description
Tix, the Tk Interface eXtension, is a powerful set of user interface
components that expands the capabilities of your Tcl/Tk and Python
applications. Using Tix together with Tk will greatly enhance the
appearance and functionality of your application.

%package devel
Summary: development files for tix
Requires: %{name} = %{epoch}:%{version}-%{release}

%description devel
Lib and head files which are needed for tix

%package        help
Summary:        Documents for tix
Buildarch:      noarch
Requires:       man info
Requires:       %{name} = %{epoch}:%{version}-%{release}

Obsoletes:     %{name}-doc < %{epoch}:%{version}-%{release}
Provides:      %{name}-doc = %{epoch}:%{version}-%{release}

%description help
Man pages and other related documents for tix.

%prep
%autosetup -p1 -n Tix%{version}
chmod ugo-x docs/html/gif/%{name}/*.png docs/html/gif/%{name}/*.gif docs/html/gif/%{name}/*/*.gif
sed -i 's/\r//' docs/Release-8.4.0.txt

%build
%configure --with-tcl=%{_libdir} --with-tk=%{_libdir} --libdir=%{_libdir}/tcl%{tcl_version}
%make_build all PKG_LIB_FILE=libTix.so

%install
%make_install PKG_LIB_FILE=libTix.so

mv %{buildroot}%{_libdir}/tcl%{tcl_version}/Tix%{version}/libTix.so %{buildroot}%{_libdir}/tcl%{tcl_version}
pwd

ln -sf ../libTix.so %{buildroot}%{_libdir}/tcl%{tcl_version}/Tix%{version}/libTix.so
ln -sf tcl%{tcl_version}/Tix%{version}/libTix.so %{buildroot}%{_libdir}/libTix.so
ln -sf tcl%{tcl_version}/Tix%{version}/libTix.so %{buildroot}%{_libdir}/libtix.so
install -d %{buildroot}%{_datadir}/tcl%{tcl_version}/Tix8.4
cp -a demos %{buildroot}%{_datadir}/tcl%{tcl_version}/Tix8.4
install -d %{buildroot}%{_includedir}
install -m 0644 generic/%{name}.h %{buildroot}%{_includedir}/%{name}.h
install -d %{buildroot}%{_mandir}/mann
cp man/*.n %{buildroot}%{_mandir}/mann
install -d %{buildroot}%{_sysconfdir}/ld.so.conf.d
echo "%{_libdir}/tcl%{tcl_version}" > %{buildroot}%{_sysconfdir}/ld.so.conf.d/%{name}-%{_arch}.conf

find docs -name .cvsignore -exec rm '{}' ';'

%ldconfig_scriptlets

%files
%{_libdir}/tcl%{tcl_version}/libTix.so
%{_libdir}/tcl%{tcl_version}/Tix%{version}
%{_sysconfdir}/ld.so.conf.d/*
%doc license.terms

%files devel
%{_includedir}/%{name}.h
%{_libdir}/libtix.so
%{_libdir}/libTix.so

%files help
%doc docs/*
%doc %{_datadir}/tcl%{tcl_version}/Tix8.4
%{_mandir}/mann/*.n*
%doc *.txt *.html

%changelog
* Tue Nov 29 2022 wuzx<wuzx1226@qq.com> - 1:8.4.3-27
- Add sw64 architecture

* Fri Dec 6 2019 caomeng<caomeng5@huawei.com> - 1:8.4.3-26
- Package init
